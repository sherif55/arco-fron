import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { PagesModule } from './pages/pages.module';
import { routing } from './app.routing';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OrderModule } from 'ngx-order-pipe';
import { CommonModule } from '@angular/common';
import { AuthService } from './shared/services/auth.service';
import { AuthGuard } from './pages/authguard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {AuthHttpInterceptor} from "./shared/http/auth-http-interceptor";
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import {CountriesService} from "./shared/services/countries.service";


import {ToasterModule, ToasterService} from 'angular2-toaster';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    PagesModule,
    CommonModule,
    routing,
    RouterModule,
    OrderModule,
    ToastModule,ToasterModule
  ],
  declarations: [
    AppComponent,
  ],
  providers: [
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: AuthHttpInterceptor, 
      multi: true 
    },
    AuthService,
    AuthGuard,
    CountriesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
