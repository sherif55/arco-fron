import { AuthService } from './../shared/services/auth.service';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './authguard';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './pages.routing';

import { LayoutModule } from '../shared/layout.module';
import { SharedModule } from '../shared/shared.module';

/* components */
import { PagesComponent } from './pages.component';
import { LoginComponent } from './login/login.component';
import { LoginmeComponent } from './loginme/loginme.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        SharedModule,
        routing,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    declarations: [
        PagesComponent,
        LoginComponent,
        LoginmeComponent,
    ],
    providers: [
        AuthService,
        HttpClient,
    ],
})
export class PagesModule { }
