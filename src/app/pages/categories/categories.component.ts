import { Component, OnInit } from '@angular/core';
import { CategoriesService } from './categories.service';
import { ToastsManager } from 'ng2-toastr/src/toast-manager';
import { ToasterService } from 'angular2-toaster/src/toaster.service';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  public order = "";
  public checkedList = [];

  public categories = [];

  public currentCategory = {
    id: "",
    name: "",
    description: ""
  };

  public tempCategory;

  public newCategory = {
    name: "",
    description: ""
  };


  constructor(private catService: CategoriesService, private toaster: ToasterService) { }

  ngOnInit() {

    $(document).ready(function () {
      //this is JQ only for right sidebar
      if ($(".pages-content").hasClass("open-r-sidebar")) {
        $(".right-sbar").addClass("open-right-sbar")
      }
      else {
        $(".right-sbar").removeClass("open-right-sbar")
      }

      $('#add-new').click(function () {
        // $('#user_options').toggle();
        // console.log("Adding");
        $(".pages-content").toggleClass("open-r-sidebar");
        $(".right-sbar#sbar-add").toggleClass("open-right-sbar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");

        if ($(".right-sbar#sbar-add,.right-sbar#sbar-edit").hasClass("open-right-sbar")) {
          $(".pages-content").addClass("open-r-sidebar")
        }
        else {
          $(".pages-content").removeClass("open-r-sidebar")
        }

      });

      $(".pages-content").on("click", '.edit-new', function () {
        // $('#user_options').toggle();
        // console.log("Editing");
        $(".pages-content").toggleClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").toggleClass("open-right-sbar");
        $(".right-sbar#sbar-add").removeClass("open-right-sbar");

        if ($(".right-sbar#sbar-add,.right-sbar#sbar-edit").hasClass("open-right-sbar")) {
          $(".pages-content").addClass("open-r-sidebar")
        }
        else {
          $(".pages-content").removeClass("open-r-sidebar")
        }

      });

      //close right sidebar for add
      $('#close-r-sidebar,.option-btn > .aroc-btn-2').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-add").removeClass("open-right-sbar");
      });

      //close right sidebar for edit
      $('#close-r-sidebar2').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");
      });

      //close right sidebar for on routes
      $('.menu-link,.slide-menu li a').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");
      })
    });

    this.getCategories();
  }

  getCategories() {
    this.catService.getCategories()
      .subscribe(data => {
        this.categories = data.data;
      });
  }

  getCategory(id) {
    this.catService.getCategory(id)
      .subscribe(data => {
        this.currentCategory = data.data;
      })
  }

  createCategory(category) {
    this.catService.createCategory(category)
      .subscribe(data => {
        this.categories.push(data.data);
        this.newCategory.name = "";
        this.newCategory.description = "";
        this.toaster.pop("success", "Success", "Successfully created");
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar").removeClass("open-right-sbar");
      })
  }


  editCategory(category) {
    console.log(category);
    this.currentCategory = JSON.parse(JSON.stringify(category));
    this.tempCategory = category;
  }

  updateCategory(category) {
    this.catService.updateCategory(category.id, category)
      .subscribe(data => {
        // TODO update the category
        let ind = this.categories.findIndex(function (item) {
          return item.id == category.id;
        });

        console.log(ind);
        this.categories[ind] = category;
        this.toaster.pop("success", "Success", "Successfully Edited");
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar").removeClass("open-right-sbar");
        
      })
  }

  
  deleteCategory() {

  }

 
  confirmDeleteCategory() {
    console.log(this.checkedList);
    this.catService.bulkDeleteCategories({ ids: this.checkedList })
      .subscribe(data => {
        console.log(data.data);
        // TODO toast if any undeleted
        if (data.data.exceptions) {
          this.toaster.pop("warning", "Error occured", "Not all categories were deleted");
        } else {
          this.toaster.pop("success", "Success", "Successfully deleted");
        }
        if (data.data.deleted_ids) {
          this.categories = this.categories.filter(function (cat) {
            return !data.data.deleted_ids.includes(cat.id);
          })

        }
      })
  }


  onCheckboxChange(option, event) {
    if (event.target.checked) {
      this.checkedList.push(option.id);
    } else {
      for (var i = 0; i < this.categories.length; i++) {
        if (this.checkedList[i] == option.id) {
          this.checkedList.splice(i, 1);
        }
      }
    }
  }

}
