import { Routes, RouterModule } from '@angular/router';
import { CategoriesComponent } from './categories.component';

const childRoutes: Routes = [
    {
        path: '',
        component: CategoriesComponent
    }
];

export const routing = RouterModule.forChild(childRoutes);
