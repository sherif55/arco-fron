import { CategoriesService } from './categories.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './categories.routing';
import { SharedModule } from '../../shared/shared.module';
import { CategoriesComponent } from './categories.component';
import { OrderModule } from 'ngx-order-pipe';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        routing,
        OrderModule
    ],
    declarations: [
        CategoriesComponent
    ],
    providers: [
        CategoriesService
    ]
})

export class CategoriesModule {

}
