import { Observable } from 'rxjs/Rx';
import { environment } from './../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CategoriesService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = environment.api + "/admin";
  }

  getCategories() {
    return this.http.get(this.url + "/categories")
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

  getCategory(id) {
    return this.http.get(this.url + "/categories/" + id)
      .catch((error: any) => {
        return Observable.throw(error.error || 'server error')
      })
  }

  getCategoryTemplates(id) {
    return this.http.get(this.url + "/categories/" + id + "/templates")
      .catch((error: any) => {
        return Observable.throw(error.error || 'server error')
      })
  }


  createCategory(data) {
    return this.http.post(this.url + "/categories", data)
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

  updateCategory(id, data) {
    return this.http.post(this.url + "/categories/" + id, data)
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

  deleteCategory(id) {
    return this.http.delete(this.url + "/categories/" + id)
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

  bulkDeleteCategories(ids) {
    return this.http.post(this.url + "/categories/bulk-delete", ids)
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

}
