import { Component, OnInit } from '@angular/core';
import { GroupsService } from './groups.service';
import { ToastsManager } from 'ng2-toastr/src/toast-manager';
import { ToasterService } from 'angular2-toaster/src/toaster.service';

declare var $: any;
declare var jquery: any;

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {

  public order = "";
  public checkedList = [];
  public tempGroup;

  public groups = [];

  public newGroup = {
    name: "",
    description: ""
  }

  public currentGroup = {
    name: "",
    description: ""
  }

  constructor(private _groupsServices: GroupsService, private toaster: ToasterService) {
    this.getGroups();
  }

  ngOnInit() {
    $(document).ready(function () {
      //this is JQ only for right sidebar
      if ($(".pages-content").hasClass("open-r-sidebar")) {
        $(".right-sbar").addClass("open-right-sbar")
      }
      else {
        $(".right-sbar").removeClass("open-right-sbar")
      }

      $('#add-new').click(function () {
        // $('#user_options').toggle();
        // console.log("Adding");
        $(".pages-content").toggleClass("open-r-sidebar");
        $(".right-sbar#sbar-add").toggleClass("open-right-sbar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");

        if ($(".right-sbar#sbar-add,.right-sbar#sbar-edit").hasClass("open-right-sbar")) {
          $(".pages-content").addClass("open-r-sidebar")
        }
        else {
          $(".pages-content").removeClass("open-r-sidebar")
        }

      });

       $(".pages-content > ").on("click", '.edit-new', function () {
        // $('#user_options').toggle();
        // console.log("Editing");
        $(".pages-content").toggleClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").toggleClass("open-right-sbar");
        $(".right-sbar#sbar-add").removeClass("open-right-sbar");

        if ($(".right-sbar#sbar-add,.right-sbar#sbar-edit").hasClass("open-right-sbar")) {
          $(".pages-content").addClass("open-r-sidebar")
        }
        else {
          $(".pages-content").removeClass("open-r-sidebar")
        }

      });

      //close right sidebar for add
      $('#close-r-sidebar,.option-btn > .aroc-btn-2').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-add").removeClass("open-right-sbar");
      });

      //close right sidebar for edit
      $('#close-r-sidebar2').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");
      });

      //close right sidebar for on routes
      $('.menu-link,.slide-menu li a').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");
      })
    });
    this.getGroups();
  }


  getGroups() {
    this._groupsServices.getGroups()
      .subscribe(data => {
        this.groups = data.data
      })
  }

  getGroup(id) {
    this._groupsServices.getGroup(id)
      .subscribe(data => {
        this.currentGroup = data.data
      })
  }


  createGroup(group) {
    this._groupsServices.createGroup(group)
      .subscribe(data => {
        this.groups.push(data.data);
        this.newGroup.name = "";
        this.newGroup.description = "";
        this.toaster.pop("success", "Success", "Successfully created");
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar").removeClass("open-right-sbar");
      })
  }




  editGroup(group) {
    console.log(group);
    this.currentGroup = JSON.parse(JSON.stringify(group));
    this.tempGroup = group;
  }

  updateGroup(group) {
    this._groupsServices.updateGroup(group.id, group)
      .subscribe(data => {
        // TODO update the group
        // this.currentgroup = data.data;
        let ind = this.groups.findIndex(function (item) {
          return item.id == group.id;
        });
        console.log(ind);
        this.groups[ind] = group;
        this.toaster.pop("success", "Success", "Successfully Edited");

      })
  }

  deleteGroup() {

  }

  bulkDeleteGroup() {
    console.log(this.checkedList);
    this._groupsServices.bulkDeleteGroup({ ids: this.checkedList })
      .subscribe(data => {
        console.log(data.data);
        // TODO toast if any undeleted
        if (data.data.exceptions) {
          this.toaster.pop("warning", "Error occured", "Not all groups were deleted");
        } else {
          this.toaster.pop("success", "Success", "Successfully deleted");
        }
        if (data.data.deleted_ids) {
          this.groups = this.groups.filter(function (groups) {
            return !data.data.deleted_ids.includes(groups.id);
          })

        }
      })
  }

  onCheckboxChange(option, event) {
    if (event.target.checked) {
      this.checkedList.push(option.id);
    } else {
      for (var i = 0; i < this.groups.length; i++) {
        if (this.checkedList[i] == option.id) {
          this.checkedList.splice(i, 1);
        }
      }
    }
  }


}
