import { GroupsComponent } from './groups.component';
import { Routes, RouterModule } from '@angular/router';

const childRoutes: Routes = [
    {
        path: '',
        component: GroupsComponent
    }
];

export const routing = RouterModule.forChild(childRoutes);
