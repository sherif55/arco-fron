import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class GroupsService {

    private url: string;

    constructor(private http: HttpClient) {
        this.url = environment.api + "/admin";
    }
    getGroups() {
        return this.http.get(this.url + "/staff_groups")
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    getGroup(id) {
        return this.http.get(this.url + "/staff_groups/" + id)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    createGroup(data) {
        return this.http.post(this.url + "/staff_groups", data)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    updateGroup(id, data) {
        return this.http.post(this.url + "/staff_groups/" + id, data)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    deleteGroup(id) {
        return this.http.delete(this.url + "/staff_groups/" + id)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    bulkDeleteGroup(ids) {
        return this.http.post(this.url + "/staff_groups/bulk-delete", ids)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }
}