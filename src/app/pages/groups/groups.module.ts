import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './groups.routing';
import { SharedModule } from '../../shared/shared.module';
import { GroupsService } from './groups.service';
import { GroupsComponent } from './groups.component';
import { OrderModule } from 'ngx-order-pipe';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        routing,
        OrderModule
    ],
    declarations: [
        GroupsComponent
    ],
    providers: [
        GroupsService
    ]
})
export class GroupsModule {

}
