import { TypesService } from './../types/types.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './units.routing';
import { SharedModule } from '../../shared/shared.module';
import { UnitsComponent } from './units.component';
import { UnitsService } from './units.service';
import { ZonesService } from '../zons/zones.service';
import { OrderModule } from 'ngx-order-pipe';



@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        routing,
        OrderModule
    ],
    declarations: [
        UnitsComponent
    ],
    providers:[
        UnitsService,
        ZonesService,
        TypesService
    ]
})
export class UnitsModule { 

}
