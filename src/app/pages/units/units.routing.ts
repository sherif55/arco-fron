import { Routes, RouterModule } from '@angular/router';
import { UnitsComponent } from './units.component';

const childRoutes: Routes = [
    {
        path: '',
        component: UnitsComponent
    }
];

export const routing = RouterModule.forChild(childRoutes);
