import { Observable } from 'rxjs/Rx';
import { environment } from './../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class UnitsService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = environment.api + "/admin";
  }

  getUnits() {
    return this.http.get(this.url + "/units")
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

  getUnit(id) {
    return this.http.get(this.url + "/units" + id)
      .catch((error: any) => {
        return Observable.throw(error.error || 'server error')
      })
  }


  createUnits(data) {
    return this.http.post(this.url + "/units", data)
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

  updateUnit(id, data) {
    return this.http.post(this.url + "/units/" + id, data)
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

  deleteUnits(id) {
    return this.http.delete(this.url + "/units/" + id)
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

  bulkDeleteUnits(ids) {
    return this.http.post(this.url + "/units/bulk-delete", ids)
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

}
