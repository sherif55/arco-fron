import { TypesService } from './../types/types.service';
import { UnitsService } from './units.service';
import { ZonesService } from './../zons/zones.service';
import { ZonsComponent } from './../zons/zons.component';
import { Component, OnInit } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/src/toast-manager';
import { ToasterService } from 'angular2-toaster/src/toaster.service';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.scss']
})
export class UnitsComponent implements OnInit {

  public units = [];
  public zones = [];
  public types = [];
  public checkedList = [];
  public newUnit = {
    name: "",
    description: "",
    zone_id: "",
    type_id: ""
  };

  public tempUnit;
  public currentUnit = {
    id: "",
    name: "",
    description: "",
    zone_id: "",
    type_id: ""
  };
  public order = "";

  constructor(
    private _unitsservices: UnitsService,
    private zonesService: ZonesService,
    private _typeservices: TypesService,
    private toaster: ToasterService
  ) {

  }

  ngOnInit() {

    $(document).ready(function () {
      //this is JQ only for right sidebar
      if ($(".pages-content").hasClass("open-r-sidebar")) {
        $(".right-sbar").addClass("open-right-sbar")
      }
      else {
        $(".right-sbar").removeClass("open-right-sbar")
      }

      $('#add-new').click(function () {
        // $('#user_options').toggle();
        // console.log("Adding");
        $(".pages-content").toggleClass("open-r-sidebar");
        $(".right-sbar#sbar-add").toggleClass("open-right-sbar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");

        if ($(".right-sbar#sbar-add,.right-sbar#sbar-edit").hasClass("open-right-sbar")) {
          $(".pages-content").addClass("open-r-sidebar")
        }
        else {
          $(".pages-content").removeClass("open-r-sidebar")
        }

      });

      $(".pages-content > ").on("click", '.edit-new', function () {
        // $('#user_options').toggle();
        // console.log("Editing");
        $(".pages-content").toggleClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").toggleClass("open-right-sbar");
        $(".right-sbar#sbar-add").removeClass("open-right-sbar");

        if ($(".right-sbar#sbar-add,.right-sbar#sbar-edit").hasClass("open-right-sbar")) {
          $(".pages-content").addClass("open-r-sidebar")
        }
        else {
          $(".pages-content").removeClass("open-r-sidebar")
        }

      });

      //close right sidebar for add
      $('#close-r-sidebar,.option-btn > .aroc-btn-2').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-add").removeClass("open-right-sbar");
      });

      //close right sidebar for edit
      $('#close-r-sidebar2').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");
      });

      //close right sidebar for on routes
      $('.menu-link,.slide-menu li a').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");
      })
    });

    this.getZones();
    this.getUnits();
    this.getTypes();
  }


  getZones() {
    this.zonesService.getZones()
      .subscribe(data => {
        this.zones = data.data;
      });
  }

  getUnits() {
    this._unitsservices.getUnits()
      .subscribe(data => {
        this.units = data.data;
      });
  }

  getTypes() {
    this._typeservices.getTypes()
      .subscribe(data => {
        this.types = data.data;
      });
  }

  createUnit(unit) {
    this._unitsservices.createUnits(unit)
      .subscribe(data => {
        this.units.push(data.data);
        this.newUnit.name = "";
        this.newUnit.description = "";
        this.toaster.pop("success", "Success", "Successfully created");
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar").removeClass("open-right-sbar");

      });
  }




  editUnit(unit) {
    console.log(unit);
    this.currentUnit = JSON.parse(JSON.stringify(unit));
    this.tempUnit = unit;
  }

  updateunit(unit) {
    this._unitsservices.updateUnit(unit.id, unit)
      .subscribe(data => {
        // TODO update the unit
        // this.currentunit = data.data;
        let ind = this.units.findIndex(function (item) {
          return item.id == unit.id;
        });
        console.log(ind);
        this.units[ind] = unit;
        this.toaster.pop("success", "Success", "Successfully Edited");
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar").removeClass("open-right-sbar");
      })
  }

  deleteUnit() {

  }

  bulkDeleteUnit() {
    console.log(this.checkedList);
    this._unitsservices.bulkDeleteUnits({ ids: this.checkedList })
      .subscribe(data => {
        console.log(data.data);
        // TODO toast if any undeleted
        if (data.data.exceptions) {
          this.toaster.pop("warning", "Error occured", "Not all units were deleted");
        } else {
          this.toaster.pop("success", "Success", "Successfully deleted");
        }
        if (data.data.deleted_ids) {
          this.units = this.units.filter(function (unit) {
            return !data.data.deleted_ids.includes(unit.id);
          })

        }
      })
  }

  onCheckboxChange(option, event) {
    if (event.target.checked) {
      this.checkedList.push(option.id);
    } else {
      for (var i = 0; i < this.units.length; i++) {
        if (this.checkedList[i] == option.id) {
          this.checkedList.splice(i, 1);
        }
      }
    }
  }

}
