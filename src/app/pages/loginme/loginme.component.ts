import { Router } from '@angular/router';
import { AuthService } from './../../shared/services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loginme',
  templateUrl: './loginme.component.html',
  styleUrls: ['./loginme.component.scss']
})
export class LoginmeComponent implements OnInit {
  public email;
  public password;

  constructor(private _authService: AuthService,
    private _routs: Router
  ) {

  }

  ngOnInit() {
  }

  login() {
    console.log(this.email, this.password);
    this._authService.login(
      this.email, this.password
    )
      .subscribe(data => {
        // console.log(data);
        if (data.code == 401) {

        } else {
          this._authService.setToken(data.data.token);
          this._routs.navigate(["/index"]);

        }
      })
  }

}
