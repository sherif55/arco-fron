import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginmeComponent } from './loginme.component';

describe('LoginmeComponent', () => {
  let component: LoginmeComponent;
  let fixture: ComponentFixture<LoginmeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginmeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginmeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
