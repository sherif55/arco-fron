import { Component, OnInit } from '@angular/core';
import { ChartsService } from '../charts/components/echarts/charts.service';
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [ChartsService]
})
export class IndexComponent implements OnInit {
  showloading: boolean = false;

  public AnimationBarOption;

  constructor(private _chartsService: ChartsService) { }

  ngOnInit() {
    this.AnimationBarOption = this._chartsService.getAnimationBarOption();
    
    //check if this page has right sidebar or not and remove it if it not hasclass
    if ($(".pages-content").parents(".right-sbar").length == 1) {
      $(".right-sbar").addClass("open-right-sbar")
      // YES, the child element is inside the parent
    } else {
      $(".right-sbar").removeClass("open-right-sbar");
      $(".pages-content").removeClass("open-r-sidebar");

      // NO, it is not inside
    }
  }
}
