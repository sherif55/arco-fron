import { LoginmeComponent } from './../pages/loginme/loginme.component';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './authguard';

export const childRoutes: Routes = [
    {
        path: 'loginme',
        component: LoginmeComponent,
    },
    {
        path: 'pages',
        component: PagesComponent,
        children: [
            { path: '', redirectTo: 'index', pathMatch: 'full' },
            { path: 'index', loadChildren: './index/index.module#IndexModule' },
            

            { path: 'zons', loadChildren: './zons/zons.module#ZonsModule' },
            { path: 'types', loadChildren: './types/types.module#TypesModule' },
            { path: 'units', loadChildren: './units/units.module#UnitsModule' },
            { path: 'facilites', loadChildren: './facilites/facilites.module#FacilitesModule' },

        ],

        canActivate: [AuthGuard],
    }
];

export const routing = RouterModule.forChild(childRoutes);
