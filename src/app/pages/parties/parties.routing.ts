import { Routes, RouterModule } from '@angular/router';
import { PartiesComponent } from './parties.component';

const childRoutes: Routes = [
    {
        path: '',
        component: PartiesComponent
    }
];

export const routing = RouterModule.forChild(childRoutes);
