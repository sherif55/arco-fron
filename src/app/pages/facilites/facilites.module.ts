import { FacilitesComponent } from './facilites.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './facilites.routing';
import { SharedModule } from '../../shared/shared.module';
import { OrderModule } from 'ngx-order-pipe';
import { FacilitiesService } from "./facilities.service";
import { ZonesService } from '../zons/zones.service';
import { TypesService } from '../types/types.service';



@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        routing,
        OrderModule
    ],
    declarations: [
        FacilitesComponent
    ],
    providers: [
        FacilitiesService,
        ZonesService,
        TypesService
    ]
})
export class FacilitesModule { 

}
