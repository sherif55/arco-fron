import { Routes, RouterModule } from '@angular/router';
import { FacilitesComponent } from './facilites.component';

const childRoutes: Routes = [
    {
        path: '',
        component: FacilitesComponent
    }
];

export const routing = RouterModule.forChild(childRoutes);
