import { Component, OnInit } from '@angular/core';
import { FacilitiesService } from './facilities.service';
import { ZonesService } from '../zons/zones.service';
import { TypesService } from '../types/types.service';
import { ToastsManager } from 'ng2-toastr/src/toast-manager';
import { ToasterService } from 'angular2-toaster/src/toaster.service';

declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-facilites',
  templateUrl: './facilites.component.html',
  styleUrls: ['./facilites.component.scss']
})
export class FacilitesComponent implements OnInit {
  public facilities = [];
  public zones = [];
  public types = [];
  public checkedList = [];
  public newFac = {
    name: "",
    description: "",
    zone_id: "",
    type_id: ""
  };

  public tempFac =[];
  public currentFac = {
    name: "",
    description: "",
    zone_id: "",
    type_id: ""
  };

  public order = "";
  constructor(private facService: FacilitiesService, private zonesService: ZonesService,
    private typesService: TypesService, private toaster: ToasterService) { }

  ngOnInit() {

    $(document).ready(function () {
      //this is JQ only for right sidebar
      if ($(".pages-content").hasClass("open-r-sidebar")) {
        $(".right-sbar").addClass("open-right-sbar")
      }
      else {
        $(".right-sbar").removeClass("open-right-sbar")
      }

      $('#add-new').click(function () {
        // $('#user_options').toggle();
        // console.log("Adding");
        $(".pages-content").toggleClass("open-r-sidebar");
        $(".right-sbar#sbar-add").toggleClass("open-right-sbar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");

        if ($(".right-sbar#sbar-add,.right-sbar#sbar-edit").hasClass("open-right-sbar")) {
          $(".pages-content").addClass("open-r-sidebar")
        }
        else {
          $(".pages-content").removeClass("open-r-sidebar")
        }

      });

      $(".pages-content > ").on("click", '.edit-new', function () {
        // $('#user_options').toggle();
        // console.log("Editing");
        $(".pages-content").toggleClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").toggleClass("open-right-sbar");
        $(".right-sbar#sbar-add").removeClass("open-right-sbar");

        if ($(".right-sbar#sbar-add,.right-sbar#sbar-edit").hasClass("open-right-sbar")) {
          $(".pages-content").addClass("open-r-sidebar")
        }
        else {
          $(".pages-content").removeClass("open-r-sidebar")
        }

      });

      //close right sidebar for add
      $('#close-r-sidebar,.option-btn > .aroc-btn-2').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-add").removeClass("open-right-sbar");
      });

      //close right sidebar for edit
      $('#close-r-sidebar2').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");
      });

      //close right sidebar for on routes
      $('.menu-link,.slide-menu li a').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");
      })
    });

    this.getZones();
    this.getFacilities();
    this.getTypes();
  }

  getZones() {
    this.zonesService.getZones()
      .subscribe(data => {
        this.zones = data.data;
      });
  }

  getTypes() {
    this.typesService.getTypes()
      .subscribe(data => {
        this.types = data.data;
      });
  }

  getFacilities() {
    this.facService.getFacilities()
      .subscribe(data => {
        this.facilities = data.data;
      });
  }

  getFacility(id) {
    this.facService.getFacility(id)
      .subscribe(data => {
        this.currentFac = data.data;
      });
  }

  createFacility(facilities) {
    this.facService.createFacility(facilities)
      .subscribe(data => {
        this.facilities.push(data.data);
        this.newFac.name = "";
        this.newFac.description = ""; 
        
        this.toaster.pop("success", "Success", "Successfully created");
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar").removeClass("open-right-sbar");

      });
  }

  editFacility(facility) {
    console.log(facility);
    this.currentFac = JSON.parse(JSON.stringify(facility));
    this.tempFac = facility;
  }



  updateFacility(facility) {
    this.facService.updateFacility(facility.id, facility)
      .subscribe(data => {
        // TODO update the facility
        // this.currentfacility = data.data;
        let ind = this.facilities.findIndex(function (item) {
          return item.id == facility.id;
        });

        this.zones[ind] = facility;
        this.toaster.pop("success", "Success", "Successfully Edited");
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar").removeClass("open-right-sbar");
      })
  }

  confirmDeleteFacility() {
    console.log(this.checkedList);
    this.facService.bulkDeleteFacilities({ ids: this.checkedList })
      .subscribe(data => {
        console.log(data.data);
        // TODO toast if any undeleted
        if (data.data.exceptions) {
          this.toaster.pop("warning", "Error occured", "Not all facility were deleted");
        } else {
          this.toaster.pop("success", "Success", "Successfully deleted");
        }
        if (data.data.deleted_ids) {
          this.facilities = this.facilities.filter(function (facility) {
            return !data.data.deleted_ids.includes(facility.id);
          })

        }
      })
  }


  onCheckboxChange(option, event) {
    if (event.target.checked) {
      this.checkedList.push(option.id);
    } else {
      for (var i = 0; i < this.facilities.length; i++) {
        if (this.checkedList[i] == option.id) {
          this.checkedList.splice(i, 1);
        }
      }
    }
  }

}
