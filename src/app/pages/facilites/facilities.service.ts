import { Observable } from 'rxjs/Rx';
import { environment } from './../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class FacilitiesService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = environment.api + "/admin";
  }

  getFacilities() {
    return this.http.get(this.url + "/facilities")
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

  getFacility(id) {
    return this.http.get(this.url + "/facilities" + id)
      .catch((error: any) => {
        return Observable.throw(error.error || 'server error')
      })
  }


  createFacility(data) {
    return this.http.post(this.url + "/facilities", data)
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

  updateFacility(id, data) {
    return this.http.post(this.url + "/facilities/" + id, data)
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

  deleteFacility(id) {
    return this.http.delete(this.url + "/facilities/" + id)
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

  bulkDeleteFacilities(ids) {
    return this.http.post(this.url + "/facilities/bulk-delete", ids)
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }

}
