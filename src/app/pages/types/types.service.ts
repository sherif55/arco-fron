import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class TypesService {

    private url: string;

    constructor(private http: HttpClient) {
        this.url = environment.api + "/admin";
    }

    getTypes() {
        return this.http.get(this.url + "/types")
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    getType(id) {
        return this.http.get(this.url + "/types/" + id)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    createType(data) {
        return this.http.post(this.url + "/types", data)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    updateType(id, data) {
        return this.http.post(this.url + "/types/" + id, data)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    deleteType(id) {
        return this.http.delete(this.url + "/types/" + id)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    bulkDeleteType(ids) {
        return this.http.post(this.url + "/types/bulk-delete", ids)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }
}