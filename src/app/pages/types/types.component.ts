import { Component, OnInit } from '@angular/core';
import { TypesService } from './types.service';
import { ToastsManager } from 'ng2-toastr/src/toast-manager';
import { ToasterService } from 'angular2-toaster/src/toaster.service';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-types',
  templateUrl: './types.component.html',
  styleUrls: ['./types.component.scss']
})
export class TypesComponent implements OnInit {

  public types = [];
  public currentType = {
    id: "",
    name: "",
    description: "",
    category: ""
  };
  public newType = {
    name: "",
    description: "",
    category: ""
  };
  public order = "";
  public checkedList = [];
  public tempType;
  constructor(private typesService: TypesService, private toaster: ToasterService) { }

  ngOnInit() {

    $(document).ready(function () {
      //this is JQ only for right sidebar
      if ($(".pages-content").hasClass("open-r-sidebar")) {
        $(".right-sbar").addClass("open-right-sbar")
      }
      else {
        $(".right-sbar").removeClass("open-right-sbar")
      }

      $('#add-new').click(function () {
        // $('#user_options').toggle();
        // console.log("Adding");
        $(".pages-content").toggleClass("open-r-sidebar");
        $(".right-sbar#sbar-add").toggleClass("open-right-sbar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");

        if ($(".right-sbar#sbar-add,.right-sbar#sbar-edit").hasClass("open-right-sbar")) {
          $(".pages-content").addClass("open-r-sidebar")
        }
        else {
          $(".pages-content").removeClass("open-r-sidebar")
        }

      });

      $(".pages-content > ").on("click", '.edit-new', function () {
        // $('#user_options').toggle();
        // console.log("Editing");
        $(".pages-content").toggleClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").toggleClass("open-right-sbar");
        $(".right-sbar#sbar-add").removeClass("open-right-sbar");

        if ($(".right-sbar#sbar-add,.right-sbar#sbar-edit").hasClass("open-right-sbar")) {
          $(".pages-content").addClass("open-r-sidebar")
        }
        else {
          $(".pages-content").removeClass("open-r-sidebar")
        }

      });

      //close right sidebar for add
      $('#close-r-sidebar,.option-btn > .aroc-btn-2').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-add").removeClass("open-right-sbar");
      });

      //close right sidebar for edit
      $('#close-r-sidebar2').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");
      });

      //close right sidebar for on routes
      $('.menu-link,.slide-menu li a').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");
      })
    });

    this.getTypes();
  }

  getTypes() {
    this.typesService.getTypes()
      .subscribe(data => {
        this.types = data.data;
      });
  }

  getType(id) {
    this.typesService.getType(id)
      .subscribe(data => {
        this.currentType = data.data;
      })
  }

  createType(type) {
    this.typesService.createType(type)
      .subscribe(data => {
        this.types.push(data.data);
        this.newType.name = "";
        this.newType.description = "";
        this.toaster.pop("success", "Success", "Successfully created");
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar").removeClass("open-right-sbar");
      })
  }

  editType(type) {

    console.log(type);
    this.currentType = JSON.parse(JSON.stringify(type));
    this.tempType = type;
  }

  updateType(type) {
    this.typesService.updateType(type.id, type)
      .subscribe(data => {
        // TODO update the type
        // this.currenttype = data.data;
        let ind = this.types.findIndex(function (item) {
          return item.id == type.id;
        });

        this.types[ind] = type;
        this.toaster.pop("success", "Success", "Successfully Edited");
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar").removeClass("open-right-sbar");
      })
  }

  deleteType() {

  }

  confirmDeleteType() {
    console.log(this.checkedList);
    this.typesService.bulkDeleteType({ ids: this.checkedList })
      .subscribe(data => {
        console.log(data.data);
        // TODO toast if any undeleted
        if(data.data.exceptions){
          this.toaster.pop("warning", "Error occured", "Not all zones were deleted");
        }else{
          this.toaster.pop("success", "Success", "Successfully deleted");
        }
        if (data.data.deleted_ids) {
          this.types = this.types.filter(function (zone) {
            return !data.data.deleted_ids.includes(zone.id);
          })
          
        }
      })
  }


  onCheckboxChange(option, event) {
    if (event.target.checked) {
      this.checkedList.push(option.id);
    } else {
      for (var i = 0; i < this.types.length; i++) {
        if (this.checkedList[i] == option.id) {
          this.checkedList.splice(i, 1);
        }
      }
    }
  }

}
