import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './types.routing';
import { SharedModule } from '../../shared/shared.module';
import {TypesComponent } from './types.component';
import { TypesService } from './types.service';
import { OrderModule } from 'ngx-order-pipe';



@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        routing,
        OrderModule
    ],
    declarations: [
        TypesComponent
    ],
    providers: [
        TypesService
    ]
})

export class TypesModule { 

}
