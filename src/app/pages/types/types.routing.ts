import { Routes, RouterModule } from '@angular/router';
import { TypesComponent } from './types.component';

const childRoutes: Routes = [
    {
        path: '',
        component: TypesComponent
    }
];

export const routing = RouterModule.forChild(childRoutes);
