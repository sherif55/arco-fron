import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { ZonesService } from './zones.service';
import { ToastsManager } from 'ng2-toastr/src/toast-manager';
import { ToasterService } from 'angular2-toaster/src/toaster.service';

//for JQ
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-zons',
  templateUrl: './zons.component.html',
  styleUrls: ['./zons.component.scss']
})
export class ZonsComponent implements OnInit {

  public order = "";
  public checkedList = [];

  public zones = [];

  public currentZone = {
    id: "",
    name: "",
    description: ""
  };

  public tempZone;

  public newZone = {
    name: "",
    description: ""
  }
  
  constructor(private zonesService: ZonesService, private toaster: ToasterService) {
    this.getZones();
  }

  ngOnInit() {
    $(document).ready(function () {
      //this is JQ only for right sidebar
      if ($(".pages-content").hasClass("open-r-sidebar")) {
        $(".right-sbar").addClass("open-right-sbar")
      }
      else {
        $(".right-sbar").removeClass("open-right-sbar")
      }

      $('#add-new').click(function () {
        // $('#user_options').toggle();
        // console.log("Adding");
        $(".pages-content").toggleClass("open-r-sidebar");
        $(".right-sbar#sbar-add").toggleClass("open-right-sbar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");

        if ($(".right-sbar#sbar-add,.right-sbar#sbar-edit").hasClass("open-right-sbar")) {
          $(".pages-content").addClass("open-r-sidebar")
        }
        else {
          $(".pages-content").removeClass("open-r-sidebar")
        }

      });

      $(".pages-content > ").on("click", '.edit-new', function () {
        // $('#user_options').toggle();
        // console.log("Editing");
        $(".pages-content").toggleClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").toggleClass("open-right-sbar");
        $(".right-sbar#sbar-add").removeClass("open-right-sbar");

        if ($(".right-sbar#sbar-add,.right-sbar#sbar-edit").hasClass("open-right-sbar")) {
          $(".pages-content").addClass("open-r-sidebar")
        }
        else {
          $(".pages-content").removeClass("open-r-sidebar")
        }

      });

      //close right sidebar for add
      $('#close-r-sidebar,.option-btn > .aroc-btn-2').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-add").removeClass("open-right-sbar");
      });

      //close right sidebar for edit
      $('#close-r-sidebar2').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");
      });

      //close right sidebar for on routes
      $('.menu-link,.slide-menu li a').click(function () {
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar#sbar-edit").removeClass("open-right-sbar");
      })
    });
    
    this.getZones();
  }

  getZones() {
    this.zonesService.getZones()
      .subscribe(data => {
        this.zones = data.data;
      });
  }

  getZone(id) {
    this.zonesService.getZone(id)
      .subscribe(data => {
        this.currentZone = data.data;
      })
  }

  createZone(zone) {
    this.zonesService.createZone(zone)
      .subscribe(data => {
        this.zones.push(data.data);
        this.newZone.name = "";
        this.newZone.description = "";
        this.toaster.pop("success", "Success", "Successfully created");
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar").removeClass("open-right-sbar");
      })
  }

  editZone(zone) {
    console.log(zone);
    this.currentZone = JSON.parse(JSON.stringify(zone));
    this.tempZone = zone;
  }

  updateZone(zone) {
    this.zonesService.updateZone(zone.id, zone)
      .subscribe(data => {
        // TODO update the zone
        // this.currentZone = data.data;
        let ind = this.zones.findIndex(function (item) {
          return item.id == zone.id;
        });

        this.zones[ind] = zone;
        this.toaster.pop("success", "Success", "Successfully Edited");
        $(".pages-content").removeClass("open-r-sidebar");
        $(".right-sbar").removeClass("open-right-sbar");
      })
  }

  deleteZone() {

  }

  confirmDeleteZone() {
    console.log(this.checkedList);
    this.zonesService.bulkDeleteZone({ ids: this.checkedList })
      .subscribe(data => {
        console.log(data.data);
        // TODO toast if any undeleted
        if(data.data.exceptions){
          this.toaster.pop("warning", "Error occured", "Not all zones were deleted");
        }else{
          this.toaster.pop("success", "Success", "Successfully deleted");
        }
        if (data.data.deleted_ids) {
          this.zones = this.zones.filter(function (zone) {
            return !data.data.deleted_ids.includes(zone.id);
          })
          
        }
      })
  }

  onCheckboxChange(option, event) {
    if (event.target.checked) {
      this.checkedList.push(option.id);
    } else {
      for (var i = 0; i < this.zones.length; i++) {
        if (this.checkedList[i] == option.id) {
          this.checkedList.splice(i, 1);
        }
      }
    }
  }

}