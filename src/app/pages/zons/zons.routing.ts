import { Routes, RouterModule } from '@angular/router';
import { ZonsComponent } from './zons.component';

const childRoutes: Routes = [
    {
        path: '',
        component: ZonsComponent
    }
];

export const routing = RouterModule.forChild(childRoutes);
