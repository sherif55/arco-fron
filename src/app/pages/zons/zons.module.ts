import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './zons.routing';
import { SharedModule } from '../../shared/shared.module';
import { ZonsComponent } from './zons.component';
import { ZonesService } from './zones.service';
import { OrderModule } from 'ngx-order-pipe';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        routing,
        OrderModule
    ],
    declarations: [
        ZonsComponent
    ],
    providers: [
        ZonesService
    ]
})
export class ZonsModule { 

}
