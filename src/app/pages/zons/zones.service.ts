import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ZonesService {

    private url: string;

    constructor(private http: HttpClient) {
        this.url = environment.api + "/admin";
    }

    getZones() {
        return this.http.get(this.url + "/zones")
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    getZone(id) {
        return this.http.get(this.url + "/zones/" + id)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    createZone(data) {
        return this.http.post(this.url + "/zones", data)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    updateZone(id, data) {
        return this.http.post(this.url + "/zones/" + id, data)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    deleteZone(id) {
        return this.http.delete(this.url + "/zones/" + id)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }

    bulkDeleteZone(ids) {
        return this.http.post(this.url + "/zones/bulk-delete", ids)
            .catch((error: any) => {
                return Observable.throw(error.error || 'Server error');
            })
    }
}