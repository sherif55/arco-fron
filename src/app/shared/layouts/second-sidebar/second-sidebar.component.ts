import { Component, OnInit } from '@angular/core';
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-second-sidebar',
  templateUrl: './second-sidebar.component.html',
  styleUrls: ['./second-sidebar.component.scss']
})
export class SecondSidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    // secondery sidebar
    var menuLeft = $('#s-l-sidebar');
    menuLeft.slideMenu({
      position: 'left',
      // submenuLinkAfter: '<i style="margin-left:10px" class="fa fa-angle-double-left"></i>',
      // backLinkBefore: '<i style="margin-right:10px" class="fa fa-angle-double-right"></i>'
    });
  }

}
