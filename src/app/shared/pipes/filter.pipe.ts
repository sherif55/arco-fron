import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: any, term, prop): any {
    console.log(term, prop);
    if(!prop){
      prop = "name";
    }
    
    if (typeof term !== "undefined" && term != "") {
      return items.filter(item => {
        return item[prop].includes(term);
      });
    } else {
      return items;
    }
  }

}
