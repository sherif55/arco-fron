import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import "rxjs/add/operator/catch";
import { Injector } from '@angular/core';
@Injectable()


export class AuthService {

    public http;

    constructor(private injector: Injector) {
        setTimeout(() => this.http = injector.get(HttpClient));
    }

    getToken() {
        return localStorage.getItem("auth_token")
    }

    isAuthenticated() {
        var token = this.getToken();
        if (token) {
            return true
        }
        else {
            return false
        }
    }

    setToken(token) {
        localStorage.setItem("auth_token", token)
    }

    removeToken() {
        localStorage.removeItem("auth_token")
    }

    logOut() {
        this.removeToken();
    }
    login(email, password) {
        return this.http.post("http://52.174.22.188/arco/public/api/admin/auth", {email:email,password:password} )
            .catch((error: any) => {
                return Observable.throw(error.json());
            })
    }
}