import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class CountriesService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = environment.api + "/admin";
  }

  getCountries() {
    return this.http.get(this.url + "/countries")
      .catch((error: any) => {
        return Observable.throw(error.error || 'Server error');
      })
  }
}
