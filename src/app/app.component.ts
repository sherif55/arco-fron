import { Component, OnInit } from '@angular/core';

declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-root',
  template: `
  <toaster-container></toaster-container>
  <router-outlet></router-outlet>
  `
})
export class AppComponent {

  ngOnIn() {
  }

}
